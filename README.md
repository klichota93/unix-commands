### Welcome to my recruitment task solution :)

I chose to implement ls and grep Unix commands using Kotlin, Spring Boot, and Spock for tests. The scope of implemented commands is of course simplified.

My main idea is to show you how I think about code/feature I implement. That's why I used hexagonal architecture. Adapters which I implemented are for CLI and local file system, but I can easily imagine that in the future some others can be required, like REST/FTP/S3 etc.

I did now write all the unit tests I would like due to lack of time. Some integration tests would be also nice to see.

To run the code, you can use Intellij and provide ls/grep commands as program arguments or you can run JAR directly from 'build/libs/unix-commands-0.0.1-SNAPSHOT.jar'.
